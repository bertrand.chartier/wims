\title{Vecteurs : Déterminant 2}
\author{C. Douriez}
\computeanswer{no}
\integer{x1=randint(-10..10)}
\integer{x2=\x1+randint(1..8)*randint(-1,1)}
\integer{y1=randint(-10..10)}
\integer{y2=randint(-10..10)}
\integer{x3=\x1+randint(1..12)*randint(-1,1)}
\integer{x4=randint(-10..10)}
\integer{y3=randint(-10..10)}
\integer{y4=\y1+randint(1..12)*randint(-1,1)}
\integer{xvec1=\x2-\x1}
\integer{yvec1=\y2-\y1}
\integer{xvec2=\x4-\x3}
\integer{yvec2=\y4-\y3}
\integer{det=\xvec1*\yvec2-\xvec2*\yvec1}
\matrix{points=A,A,C,B,E,M,R
B,B,D,C,F,N,S
C,E,E,D,G,P,T
D,F,F,E,H,R,U
}
\integer{choix=random(1..7)}
\statement{Dans le plan muni d'un repère, on donne \points[1;\choix](\x1 ; \y1), \points[2;\choix](\x2 ; \y2) , \points[3;\choix](\x3 ; \y3) et \points[4;\choix](\x4 ; \y4).<br>
Calculer le déterminant des vecteurs 
\(\overrightarrow{\points[1;\choix]\points[2;\choix]})  et \(\overrightarrow{\points[3;\choix]\points[4;\choix]}) .
}
\answer{Déterminant}{\det}{type=numeric}
\feedback{1=1}{\(\overrightarrow{\points[1;\choix]\points[2;\choix]}) a pour coordonnées  \(\left ( \begin{array}{c}\x2-(\x1)\\\y2-(\y1)\end{array} \right )=\left ( \begin{array}{c}\xvec1\\\yvec1\end{array} \right )) et le vecteur 
\(\overrightarrow{\points[3;\choix]\points[4;\choix]}) a pour coordonnées  \(\left ( \begin{array}{c}\xvec2\\\yvec2\end{array} \right )) . Le déterminant est \(\xvec1 \times (\yvec2) -(\xvec2) \times (\yvec1)=\det).}