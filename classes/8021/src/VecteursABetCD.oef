\title{Vecteurs : (AB) et (CD) parall�les?}
\author{C. Douriez}
\computeanswer{no}
\integer{x1=random(1..20)*random(-1,1)}
\integer{x4=random(1..20)*random(-1,1)}
\integer{x3=random(-20..20)}
\integer{y1=random(-20..20)}
\integer{y4=random(-20..20)}
\integer{y3=random(-20..20)}
\integer{coef=random(2..5)*random(-1,1)}
\integer{t=randint(1,2)}
\integer{q=randint(1,2)}
\integer{x2=(\t=1)?\coef*(\x4-\x3)+\x1 : (2-\q)*(\coef*(\x4-\x3)+\x1)}
\integer{y2=(\t=1)? \coef*(\y4-\y3)+\y1 : (\q+1)*(\coef*(\y4-\y3)+\y1)}
\integer{xvec1=\x2-\x1}
\integer{yvec1=\y2-\y1}
\integer{xvec2=\x4-\x3}
\integer{yvec2=\y4-\y3}
\integer{det=(\xvec1*\yvec2)-(\xvec2*\yvec1)}
\text{etapes=r1,r2
r3,r4
r5,r6,r7}
\nextstep{\etapes}
\statement{<b>Cet exercice comporte plusieurs �tapes.</b><br>Dans le plan muni d'un rep�re, on donne A(\x1 ; \y1), B(\x2 ; \y2) , C(\x3 ; \y3) et D(\x4 ; \y4).<br>
Les droites (AB) et (CD) sont-elles parall�les?<br>
Il suffit de v�rifier si les vecteurs \(\overrightarrow{AB}) et \(\overrightarrow{CD}) sont colin�aires.<br><br>
\if{\step=1}{\(\overrightarrow{AB}) a pour coordonn�es:
 \special{mathmlinput [\left ( \begin{array}{c}reply1\\reply2\end{array} \right )],2
reply1
reply2}
}
\if{\step=2}{\(\overrightarrow{CD}) a pour coordonn�es:
 \special{mathmlinput [\left ( \begin{array}{c}reply3\\reply4\end{array} \right )],2
reply3
reply4}
}
\if{\step=3}{\(\overrightarrow{AB}) a pour coordonn�es 
 \(\left ( \begin{array}{c}\xvec1\\\yvec1\end{array} \right )) et \(\overrightarrow{CD}) a pour coordonn�es
 \(\left ( \begin{array}{c}\xvec2\\\yvec2\end{array} \right )).<br>Le d�terminant de \(\overrightarrow{AB}) et \(\overrightarrow{CD}) est �gal � \embed{r5,3}<br>
donc ces vecteurs \embed{r6,4} colin�aires et les droites (AB) et (CD) \embed{r7,4} parall�les.}
}
\answer{}{\xvec1}{type=numeric}{option=nonstop}
\answer{}{\yvec1}{type=numeric}{option=nonstop}
\answer{}{\xvec2}{type=numeric}{option=nonstop}
\answer{}{\yvec2}{type=numeric}{option=nonstop}
\answer{}{\det}{type=numeric}{option=nonstop}
\answer{}{\t;sont,ne sont pas}{type=menu}{option=nonstop}
\answer{}{\t;sont,ne sont pas}{type=menu}
\solution{\(\overrightarrow{AB}) a pour coordonn�es  \(\left ( \begin{array}{c}\x2-(\x1)\\\y2-(\y1)\end{array} \right )=\left ( \begin{array}{c}\xvec1\\\yvec1\end{array} \right )) et le vecteur 
\(\overrightarrow{CD}) a pour coordonn�es  \(\left ( \begin{array}{c}\xvec2\\\yvec2\end{array} \right )) .<br>
Le d�terminant de \(\overrightarrow{AB}) et \(\overrightarrow{CD}) est \xvec1&times;(\yvec2)-(\xvec2)&times;(\yvec1)=\det\if{\det!=0}{&ne;0}<br>
donc ces vecteurs \if{\det=0}{sont}{ne sont pas} colin�aires et les droites (AB) et (CD)  \if{\det=0}{sont}{ne sont pas} parall�les.}