\title{Cube : in�quation 2: a x^3+b>c}

\language{fr}
\computeanswer{yes}
\precision{1000}
\author{Cyrille, Douriez}
\email{cyrille.douriez@ac-amiens.fr}
\format{html}

##In�quation a x^3+b<c ou similaire (c-b divisible par a et cube)##
\integer{x1=randint(-4..4)}
\integer{k=\x1^3}
\integer{x2=-\x1}
\integer{a=randint(2..5)*randint(-1,1)}
\integer{b=randint(2..15)*randint(-1,1)}
\integer{c=\k*\a+\b}
\integer{typ=randint(1..4)}
\text{ineq=&lt;,&le;,&ge;,&gt}##Type d'in�galit� finale##
\text{ineqe=(\a>0)? \ineq:&gt;,&ge;,&le;,&lt}##Type d'in�galit� de l'�nonc� (tient compte du signe de a)##
##Choix des cases et solution <,<=,>=,>##
\text{prop= &#123;,&#125;,\k,\x2,&#93;,&#91;,U,&empty;,0,+&infin;,-&infin;}
\matrix{sol=&#93;,-&infin;,&#59;,\x1,&#91;
&#93;,-&infin;,&#59;,\x1,&#93;
&#91;,\x1,&#59;,+&infin;,&#91;
&#93;,\x1,&#59;,+&infin;,&#91;
}

\statement{R�soudre dans \(\mathbb R), l'in�quation :
<div class="wimscenter wims_emph">
\a x<sup>3</sup>\if{\b>0}{+}\b\ineqe[\typ]\c</div>
S=\embed{r1,45x30}
<div class="wims_instruction">
Faire glisser les �lements afin de former l'ensemble des solutions.</div>
}
\answer{Solutions}{\sol[\typ;];\prop}{type=dragfill}


##El�ment pour le corrig�, dessin intervalle(s) solution(s)##
\if{\typ<3}{\text{seg1=-6,0,\x1,0}}{\text{seg1=\x1,0,6,0}}
\function{f=maxima(\a*x^3+\b)}

\feedback{\sc_reply1=0}{<b><u>Corrig� : </u></b>l'�quation \(\f=\c) est �quivalente � \(x^3=\k). Une solution : \x1.<br />
D'apr�s les variations de la fonction cube, on obtient (en orange) l'ensemble des solutions.
\draw{300,300}{
xrange -6,6
yrange -70,70
linewidth 2
segment -6,0,6,0, black
segment 0,-70,0,70, black
segment 1,-0.3,1,0.3, black
segment -0.3,10,0.3,10, black
text black,-0.5,0,medium,O
text black,0.8,0,medium,1
text black,-0.7,10.3,medium,10
text blue,-5,65,medium,y=x^3
segment -6,\k,6,\k,green
segment \x1,0,\x1,\k,green
segment \seg1[1],\seg1[2],\seg1[3],\seg1[4],orange
text green,0.3,\k,medium,\k
plot blue,x^3}}