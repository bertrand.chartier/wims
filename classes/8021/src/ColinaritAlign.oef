\title{Vecteurs : Alignement direct}
\language{fr}
\range{-5..5}
\author{Cyrille Douriez}
\email{cyrille.douriez@ac-amiens.fr}

\computeanswer{no}
\format{html}
\precision{10000}

\keyword{vecteurs,colin�arit�,d�terminant}

\matrix{points=A,C,D,F,M,R
B,D,E,G,N,S
C,E,F,H,P,T}
\integer{choix=randint(1..6)}

\integer{x1=randint(1..20)}
\integer{y1=randint(-10..10)}
\integer{x2=\x1+randint(1..20)*randint(-1,1)}
\integer{y2=randint(-10..10)}

\integer{v1x=\x2-\x1}
\integer{v1y=\y2-\y1}

\integer{col=randint(1,2,2)}
\real{coef=randint(-1,1)*randint(11..35)/10}
\real{x3=\x1+\v1x*\coef}
\real{y3=(\col=1)? \y1+\v1y*\coef:\y1+\v1y*\coef+randint(10..30)/10}

\real{v2x=\x3-\x1}
\real{v2y=\y3-\y1}

\real{det=\v1x*\v2y-\v2x*\v1y}

\statement{Dans un rep�re donn�, les points \(\points[1;\choix]\left( \x1 ,\y1 \right)),\(\points[2;\choix]\left( \x2 ,\y2 \right)) et \(\points[3;\choix]\left( \x3 ,\y3 \right)) sont-ils align�s ?}

\answer{Points align�s?}{\col;Oui,Non,Je ne sais pas}{type=radio}

\feedback{1=1}{\(\overrightarrow{\points[1;\choix]\points[2;\choix]}) a pour coordonn�es \(\left ( \begin{array}{c}\v1x\\\v1y\end{array}\right ) ) et \(\overrightarrow{\points[1;\choix]\points[3;\choix]}) a pour coordonn�es \(\left ( \begin{array}{c}\v2x\\\v2y\end{array}\right ) ).<br>
 Le d�terminant \(\v1x\times (\v2y)-(\v2x)\times(\v1y)=\det)\if{\det!=0}{&ne;0} donc les vecteurs \(\overrightarrow{\points[1;\choix]\points[2;\choix]}) et \(\overrightarrow{\points[1;\choix]\points[3;\choix]}) \if{\d=0}{sont colin�aires}{ne sont pas colin�aires} et les points 
(\points[1;\choix]), (\points[2;\choix]) et (\points[3;\choix]) \if{\d=0}{sont align�s}{ne sont pas align�s}.}