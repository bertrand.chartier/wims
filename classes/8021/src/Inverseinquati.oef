\title{Inverse : in�quation 2: 1/x>a}

\language{fr}
\computeanswer{yes}
\precision{1000}
\author{Cyrille, Douriez}
\email{cyrille.douriez@ac-amiens.fr}
\format{html}

##In�quation 1/x<a ou similaire ##
\real{a=randitem(-5,-4,-2,-1,-0.5,0.5,1,2,4,5)}
\integer{typ=randint(1..4)}
\text{ineq=<,\leq,\geq,>}##Type d'in�galit�##
##Choix des cases et solution##
\real{r=1/\a}
\text{prop=&#123;,&#125;,0,\a,&#93;,&#91;,U,&empty;}
\if{\a<0}{\matrix{sol=&#93;,\r,&#59;,0,&#91;
&#91;,\r,&#59;,0,&#91;
&#93;,-&infin;,&#59;,\r,&#93;,U,&#93;,0,&#59;,+&infin;,&#91;
&#93;,-&infin;,&#59;,\r,&#91;,U,&#93;,0,&#59;,+&infin;,&#91;}}
\if{\a>0}{\matrix{sol=&#93;,-&infin;,&#59;,0,&#91;,U,&#93;,\r,&#59;,+&infin;,&#91;
&#93;,-&infin;,&#59;,0,&#91;,U,&#91;,\r,&#59;,+&infin;,&#91;
&#93;,0,&#59;,\r,&#93;
&#93;,0,&#59;,\r,&#91;}}

\statement{R�soudre dans &#8477;*, l'in�quation :
<div class="wimscenter wims_emph">
\(\frac{1}{x} \ineq[\typ] \a)</div>
S=\embed{r1,45x30}
<div class="wims_instruction">
Faire glisser les �lements afin de former l'ensemble des solutions.</div>
}
\answer{Solutions}{\sol[\typ;];\prop}{type=dragfill}


##El�ment pour le corrig�, dessin intervalle(s) solution(s)##
\if{\typ<3}{\if{\a<=0}{\text{seg1=\r,0,0,0}}{\text{seg1=\r,0,5,0}}
}{\if{\a<=0}{\text{seg1=-5,0,\r,0}}{\text{seg1=0,0,\r,0}}}
\if{\a<0 and \typ>=3}{\text{seg2=0,0,5,0}}
{\if{\a>0 and \typ<=2}{\text{seg2=-5,0,0,0}}{\text{seg2=-5,-11,5,-11}}}


\feedback{\sc_reply1=0}{<b><u>Corrig� :</u></b> 0 est valeur interdite. l'�quation \(\frac{1}{x}=\a) a une solution : \(x=\frac{1}{\a}=\r).<br>
D'apr�s les variations de la fonction inverse, on obtient \if{\a>0 or \typ>=3}{(en orange)} l'ensemble des solutions.<br>
\draw{300,300}{
xrange -5,5
yrange -6,6
linewidth 2
segment -10,0,10,0, black
segment 0,-10,0,10, black
segment -10,\a,10,\a,green
segment 1,-0.3,1,0.3, black
segment -0.3,1,0.3,1, black
text black,-0.5,0,medium,O
text black,0.8,0,medium,I
text black,-0.7,1.3,medium,J
text blue,7,9.5,medium,y=1/x
segment 1/\a,\a, 1/\a,0,green
text green,0.3,\a,medium,\a
segment \seg1[1],\seg1[2],\seg1[3],\seg1[4],orange
segment \seg2[1],\seg2[2],\seg2[3],\seg2[4],orange
plot blue,1/x}}