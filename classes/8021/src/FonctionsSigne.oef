\title{Fonctions : Signe graphiquement}
\language{fr}
\range{-5..5}
\author{Cyrille Douriez}
\email{Cyrille.douriez@ac-amiens.fr}
\computeanswer{no}
\format{html}
\precision{100}

\real{x1=randint(-8..0)}
\real{x2=\x1+randint(4..8)}
\real{S=\x1+\x2}
\real{P=\x1*\x2}
\real{p1=min(10/(\P-0.5*\S*\S),-10/(\P-0.5*\S*\S))}
\real{p2=max(10/(\P-0.5*\S*\S),-10/(\P-0.5*\S*\S))}
\real{a=random(\p1..\p2)/2}
\function{f=maxima(expand(\a*(x-\x1)*(x-\x2)))}
\text{signe=(\a>0)? 2,1,2:1,2,1}
\text{signeS=(\a>0)? +,-,+:-,+,-}
\statement{Ci-dessous est trac�e la courbe repr�sentative d'une fonction f d�finie sur \(\mathbb{R}).
<div class="wimscenter">
\draw{300,300}{
xrange -10,10
yrange -10,10
parallel -10,-10,-10,10,1,0, 20, grey 
parallel -10,-10,10,-10,0,1, 20, grey 
hline 0,0,black
vline 0,0,black
line 1,-0.3,1,0.3, black
line -0.3,1,0.3,1, black
text black , -0.5,-0.2,small , O
text black , 1,-0.3,small , I
text black , -0.5,1,small , J
linewidth 1.5
plot blue,\f}</div> Lire graphiquement le signe de f(x):<br>
<table cellspacing="0" width="40%">
<tbody>
<tr>
<td
style="border-right: 1px solid; border-bottom: 1px solid;text-align: center;" width="35%">\(x)
</td>
<td style="border-bottom: 1px solid;text-align: left; width: 5%">-&infin; </td>
<td style="border-bottom: 1px solid;text-align: center; width: 10%"></td>
<td style="border-bottom: 1px solid;text-align: center; width: 5%">\embed{r1,2}</td>
<td style="border-bottom: 1px solid;text-align: center; width: 10%"></td>
<td style="border-bottom: 1px solid;text-align: center; width: 5%">\embed{r2,2}</td>
<td style="border-bottom: 1px solid;text-align: center; width: 10%"></td>
<td style="border-bottom: 1px solid;text-align: right; width: 5%">+&infin;</td>
</tr>
<tr>
<td style="border-right: 1px solid;text-align: center;">\(f(x))</td>
<td style="text-align: left; width: 5%"></td>
<td style="text-align: center; width: 10%">\embed{r3,2}</td>
<td style="text-align: center; width: 5%">0</td>
<td style="text-align: center; width: 10%">\embed{r4,2}</td>
<td style="text-align: center; width: 5%">0</td>
<td style="text-align: center; width: 10%">\embed{r5,2}</td>
<td style="text-align: right; width: 5%"></td>
</tr>
</tbody>
</table>
}

\answer{}{\x1}{type=numeric}
\answer{}{\x2}{type=numeric}
\answer{}{\signe[1];-,+}{type=menu}
\answer{}{\signe[2];-,+}{type=menu}
\answer{}{\signe[3];-,+}{type=menu}

\feedback{1=1}{<table cellspacing="0" width="40%">
<tbody>
<tr>
<td
style="border-right: 1px solid; border-bottom: 1px solid;text-align: center;" width="35%">\(x)
</td>
<td style="border-bottom: 1px solid;text-align: left; width: 5%">-&infin; </td>
<td style="border-bottom: 1px solid;text-align: center; width: 10%"></td>
<td style="border-bottom: 1px solid;text-align: center; width: 5%">\x1</td>
<td style="border-bottom: 1px solid;text-align: center; width: 10%"></td>
<td style="border-bottom: 1px solid;text-align: center; width: 5%">\x2</td>
<td style="border-bottom: 1px solid;text-align: center; width: 10%"></td>
<td style="border-bottom: 1px solid;text-align: right; width: 5%">+&infin;</td>
</tr>
<tr>
<td style="border-right: 1px solid;text-align: center;">\(f(x))</td>
<td style="text-align: left; width: 5%"></td>
<td style="text-align: center; width: 10%">\signeS[1]</td>
<td style="text-align: center; width: 5%">0</td>
<td style="text-align: center; width: 10%">\signeS[2]</td>
<td style="text-align: center; width: 5%">0</td>
<td style="text-align: center; width: 10%">\signeS[3]</td>
<td style="text-align: right; width: 5%"></td>
</tr>
</tbody>
</table>}