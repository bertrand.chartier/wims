\title{Vecteurs : Calcul des coordonn�es-operations}
\author{C. Douriez}
\computeanswer{no}
\text{P1=A,B,C,E,F,M,P}
\text{P2=B,C,D,F,G,N,R}
\text{P3=C,D,E,G,H,P,N}
\integer{choix=randint(1..7)}
\integer{x1=randint(-10..10)}
\integer{x2=\x1+randint(1..8)*randint(-1,1)}
\integer{x3=randint(-10..10)}
\integer{y1=randint(-10..10)}
\integer{y2=randint(-10..10)}
\integer{y3=\y1+randint(1..8)*randint(-1,1)}
\integer{coef1=randint(1,1,1,2,3)}
\integer{coef2=randint(2..5)*randint(-1,1)}
\integer{solx=\coef1*(\x2-\x1)+\coef2*(\x3-\x1)}
\integer{soly=\coef1*(\y2-\y1)+\coef2*(\y3-\y1)}
##Ci-dessous �l�ments pour le corrig�##
\integer{vec1x=(\x2-\x1)}
\integer{vec1y=(\y2-\y1)}
\integer{vec11x=\coef1*(\x2-\x1)}
\integer{vec11y=\coef1*(\y2-\y1)}
\integer{vec2x=(\x3-\x1)}
\integer{vec2y=(\y3-\y1)}
\integer{coef2a=abs(\coef2)}
\integer{vec21x=\coef2a*(\x3-\x1)}
\integer{vec21y=\coef2a*(\y3-\y1)}





\statement{Calculer les coordonn�es du vecteur \if{\coef1>1}{\(\coef1)}\(\overrightarrow{\P1[\choix]\P2[\choix]})\if{\coef2>0}{\({}+{})}\(\coef2 \overrightarrow{\P1[\choix]\P3[\choix]}) avec \P1[\choix](\x1; \y1); \P2[\choix](\x2; \y2) et \P3[\choix](\x3; \y3):
<br>\if{\coef1>1}{\(\coef1)}\(\overrightarrow{\P1[\choix]\P2[\choix]})\if{\coef2>0}{\({}+{})}\(\coef2 \overrightarrow{\P1[\choix]\P3[\choix]})
a pour coordonn�es \special{mathmlinput [\left ( \begin{array}{c}
reply1\\
reply2 
\end{array} \right )],2
reply1,2
reply2,2
}
}



\answer{}{\solx}{type=numeric}
\answer{}{\soly}{type=numeric}
\hint{Les coordonn�es d'un vecteur \(\overrightarrow{AB}) sont \(\left ( \begin{array}{c}x_B-x_A\\y_B-y_A\end{array}\right )).<br> Les op�rations sur les vecteurs s'appliquent aussi � leurs coordonn�es.}
\solution{Les coordonn�es du vecteur \(\overrightarrow{\P1[\choix]\P2[\choix]}) sont \(\left ( \begin{array}{c}
\x2-(\x1)\\
\y2-(\y1)
\end{array} \right )=\left ( \begin{array}{c}
\vec1x\\
\vec1y
\end{array} \right ))
\if{\coef1>1}{donc les coordonn�es de \(\coef1 \overrightarrow{\P1[\choix]\P2[\choix]}) sont  \(\left ( \begin{array}{c}
\vec11x\\
\vec11y
\end{array} \right ))}.<br>
Les coordonn�es du vecteur \(\overrightarrow{\P1[\choix]\P3[\choix]})  sont \(\left ( \begin{array}{c}
\vec2x\\
\vec2y
\end{array} \right ))
 donc les coordonn�es de \(\coef2a \overrightarrow{\P1[\choix]\P3[\choix]}) sont  \(\left ( \begin{array}{c}
\vec21x\\
\vec21y
\end{array} \right )).<br>
D'o�, les coordonn�es de  \if{\coef1>1}{\(\coef1)}\(\overrightarrow{\P1[\choix]\P2[\choix]})\if{\coef2>0}{\({}+{})}\(\coef2 \overrightarrow{\P1[\choix]\P3[\choix]}) sont 
\(\left ( \begin{array}{c}
\solx\\
\soly
\end{array} \right ))}