#!/bin/bash

#Access SSH
mkdir ~/.ssh/
cat > ~/.ssh/authorized_keys <<'EOF'
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDkmtT9axlgswObzw0REihyRq18ecltOqUHKwETWNSbFpZ+aOdt4zu++tg2OEgrO84r97YFDabrNbhCZRLDanm8j3g35RRvSZLc6mpkqEwXAXrUi4+nL0j7ZlddGFx+lQIYEfAXnegbpfwt4E6zYzX2CQG7s02QAZfaKbDEFxzs18jMfojtaMePOnth3bKuhvD4CgQ8mtzG/BevDlG9qeIk+WQbZBDuSXGlByrrwVhaipHGpIbZ/XpWqF9PTcUdPeWVK7N/9WjMgCwwN/4LJQuDu2nOozxvSyOSEtdaDp6nBMZg0D0abviztJtCqpsnDCh7nejGlQDaTh5PtGMqGWkV root@deb10Wims

EOF

sed -i -e "s/dhcp/static/g" /etc/network/interfaces
cat >> /etc/network/interfaces <<'EOF'
address 192.168.0.102
netmask 255.255.255.0
network 192.168.0.0
broadcast 192.168.0.255
gateway 192.168.0.254
dns-nameservers 192.168.0.254

EOF

#Updates
apt-get install -y unattended-upgrades
dpkg-reconfigure unattended-upgrades
sed -i -e "s/\/\/Unattended-Upgrade::Mail \"root\"\;/Unattended-Upgrade::Mail \"root\"\;/g" /etc/apt/apt.conf.d/50unattended-upgrades


#Envoi Mail
apt-get install -y exim4
sed -i -e "s/bch/bertrand.chartier+rootdeb9wims@ac-grenoble.fr/g" /etc/aliases
echo deb9wims.local > /etc/mailname

cat > /etc/exim4/update-exim4.conf.conf <<'EOF'
# /etc/exim4/update-exim4.conf.conf
#
# Edit this file and /etc/mailname by hand and execute update-exim4.conf
# yourself or use 'dpkg-reconfigure exim4-config'
#
# Please note that this is _not_ a dpkg-conffile and that automatic changes
# to this file might happen. The code handling this will honor your local
# changes, so this is usually fine, but will break local schemes that mess
# around with multiple versions of the file.
#
# update-exim4.conf uses this file to determine variable values to generate
# exim configuration macros for the configuration file.
#
# Most settings found in here do have corresponding questions in the
# Debconf configuration, but not all of them.
#
# This is a Debian specific file

dc_eximconfig_configtype='smarthost'
dc_other_hostnames=''
dc_local_interfaces='127.0.0.1 ; ::1'
dc_readhost='deb9Wims.local'
dc_relay_domains=''
dc_minimaldns='false'
dc_relay_nets=''
dc_smarthost='smtp.ac-grenoble.fr::25'
CFILEMODE='644'
dc_use_split_config='false'
dc_hide_mailname='false'
dc_mailname_in_oh='true'
dc_localdelivery='mail_spool'

EOF

cat >> /etc/email-addresses <<'EOF'
root : bertrand.chartier+rootdeb9wims@ac-grenoble.fr
wims : bertrand.chartier+wimsdeb9wims@ac-grenoble.fr
EOF

#Alerte Accès
cat > /etc/ssh/sshrc <<'EOF'
ip=`echo $SSH_CONNECTION | cut -d " " -f 1`
hostres=`host $ip | grep "not found"`
nom=`hostname`
if [ "${hostres}" = "" ]; then
 source_hostname=`host $ip |cut -d " " -f 5`
 echo "ALERTE sshrc host - Acces en tant que $USER en shell sur $nom le : " `date` $source_hostname | mail -s "[$nom] [SSH] Alerte : Acces de $USER depuis $source_hostname" bertrand.chartier+rootdeb9wims@ac-grenoble.fr
else
 echo "ALERTE sshrc - Acces en tant que $USER en shell sur $nom le : " `date` $ip | mail -s "[$nom] [SSH] Alerte : Acces de $USER depuis $ip" bertrand.chartier+rootdeb9wims@ac-grenoble.fr
fi

EOF

update-exim4.conf 
service exim4 restart

#Serveur temps
apt-get install -y ntp
sed -i -e "s/pool 0.debian.pool.ntp.org iburst/ntp.unice.fr prefer\npool 0.debian.pool.ntp.org iburst/g" /etc/ntp.conf

#SSL
openssl req -new -x509 -days 365 -nodes -out /etc/ssl/certs/wimsserver.crt -keyout /etc/ssl/private/wimsserver.key
chmod 440 /etc/ssl/private/wimsserver.key









