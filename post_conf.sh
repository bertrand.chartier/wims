#!/bin/bash

# Limitation aux adresses ac-grenoble
sed -i -e "s/versailles/grenoble/g" /opt/wims/public_html/modules/adm/class/regclass/proc/newclass.phtml
sed -i -e "s/versailles/grenoble/g" /opt/wims/public_html/modules/adm/class/regclass/proc/newclass.proc
sed -i -e "s/versailles/grenoble/g" /opt/wims/public_html/modules/adm/manage/lang/confdata.fr

# Place le CAS comme choix par défaut d'authentification
sed -i -e "s/other,$auth_cas_url_list/$auth_cas_url_list,other/g" /opt/wims/public_html/modules/adm/class/regclass/proc/newclass.phtml
sed -i -e "s/$wims_name_otherurl,$auth_cas_name_list/$auth_cas_name_list,$wims_name_otherurl/g" /opt/wims/public_html/modules/adm/class/regclass/proc/newclass.phtml

#Personnalisation du site
mv wims/themes/Planete /opt/wims/public_html/themes/
chown -R wims:wims /opt/wims/public_html/themes/Planete/
#chmod '0700' /opt/wims/public_html/themes/

# Copie du fichier de configuration du site
mv wims/log/wims.conf	/opt/wims/log/wims.conf
chown -R wims:wims /opt/wims/log/wims.conf
chmod '0600' /opt/wims/log/wims.conf


# Classes ouvertes COVID
cd /opt/wims
wget http://wimstest1.di.u-psud.fr/wims/cl.tgz
tar xzf cl.tgz
rm cl.tgz
wget https://wimstest1.di.u-psud.fr/wims/8012.tgz
tar xzf 8012.tgz
rm 8012.tgz
chown -R wims:wims /opt/wims/log/classes
chmod '0700' /opt/wims/log/classes
cd /opt/wims/log/classes
./.build-index

find -type d -regextype sed -regex ".*/[0-9]\{4\}" >> classe_ouverte.txt
# sed -i -e "s/set class_theme=standard/set class_theme=Planete/g"
