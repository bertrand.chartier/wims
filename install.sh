#!/bin/bash

apt-get update
apt-get install -y apt-transport-https gnupg
echo deb https://apt.ac-versailles.fr/ bullseye main > /etc/apt/sources.list.d/wims.list
wget https://apt.ac-versailles.fr/public-key.pgp -O - | apt-key add -
apt-get update
apt-get install -y wims

#Exclure wims des mises à jour pour éviter l'écrasement du post-conf.
apt-mark hold wims

# Configuration de Apache
cat > /etc/apache2/sites-available/wims.conf <<'EOF'

<VirtualHost *:80>
    # Redirection HTTP to HTTPS.
    Redirect permanent / https://wims.ac-grenoble.fr/wims/
</VirtualHost>


<IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        RedirectMatch permanent ^/$ /wims/

        SSLEngine             on
        SSLCertificateFile    /etc/ssl/certs/wimsserver.crt
        SSLCertificateKeyFile /etc/ssl/private/wimsserver.key

        <FilesMatch "\.(cgi|shtml|phtml|php)$">
            SSLOptions +StdEnvVars
        </FilesMatch>

        <Directory /usr/lib/cgi-bin>
            SSLOptions +StdEnvVars
        </Directory>
    </VirtualHost>
</IfModule>

EOF

#rm /etc/apache2/sites-enabled/default
a2enmod ssl
a2ensite wims.conf
service apache2 restart
